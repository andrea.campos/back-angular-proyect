// Librería
const mongoose = require("mongoose");
// Dentro de mongoose
const Schema = mongoose.Schema;

// Define un schema -> la estructura que nos devuelva la Db (data base)
const homeSchema = new Schema({
    name: { type: String, required: true },
    lastname: { type: String, required: true },
    description: { type: String, required: true },
});


// Exportamos el modelo
const home = mongoose.model("home", homeSchema);
module.exports = home;