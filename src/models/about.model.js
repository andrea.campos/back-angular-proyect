// Librería
const mongoose = require("mongoose");
// Dentro de mongoose
const Schema = mongoose.Schema;

// Define un schema -> la estructura que nos devuelva la Db (data base)
const aboutSchema = new Schema({
    img: { type: String, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true },
});

// Exportamos el modelo
const about = mongoose.model("about", aboutSchema);
module.exports = about;