// Librería
const mongoose = require("mongoose");
// Dentro de mongoose
const Schema = mongoose.Schema;

// Define un schema -> la estructura que nos devuelva la Db (data base)
const skillsSchema = new Schema({
    img: { type: String, required: true },
    title: { type: String, required: true },
    description: { type: String, required: true },
});

// Exportamos el modelo
const skills = mongoose.model("skills", skillsSchema);
module.exports = skills;