// Librería
const mongoose = require("mongoose");
// Dentro de mongoose
const Schema = mongoose.Schema;

// Define un schema -> la estructura que nos devuelva la Db (data base)
const newMessageSchema = new Schema({
    name: { type: String, required: true, minLenght: 1, trim: true },
    mail: { type: String, required: true, trim: true },
    age: { type: Number, required: true, min: 18, trim: true },
    message: { type: String, required: true, maxlength: 100, trim: true },
});

// Exportamos el modelo
const newMessage = mongoose.model("newMessage", newMessageSchema);
module.exports = newMessage;