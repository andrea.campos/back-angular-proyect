const mongoose = require("mongoose");

const URI = "mongodb://localhost/andrea-campos";

mongoose
    .connect(URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then((db) => console.log("Db is connected"))
    .catch((err) => console.log(err));

module.exports = mongoose;