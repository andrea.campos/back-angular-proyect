const express = require("express");
const morgan = require("morgan");
const cors = require("cors");

const app = express();

// Usamos cors para arreglar la comunicacion entre distintos origins (urls) de
// Invocando cors sin argumentos, usaremos la config básica (libre)
app.use(cors());

// Requiero la config de data base y la lanzo directamente, no la asigno a nada
require('./config/database');

// app Config -> setting
const PORT = 8000;

// Process data -> middleware
app.use(morgan("dev"));
// Transforma los body a JSOn cuando llegan en POST/PUT
app.use(express.json());

// Create EndPoint -> Routes
// Paro y voy a ese fichero -> routes
app.use("/api/home", require("./routes/home.routes"));

app.use("api/about", require("./routes/about.routes"));

app.use("api/skills", require("./routes/skills.routes"));

app.use("api/newMessage", require("./routes/newMessage.routes"));

// Init app -> Create or Start app
app.listen(PORT, () => {
    console.log("app LISTEN");
});