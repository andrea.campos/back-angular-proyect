const mongoose = require("mongoose");

const Home = require("../models/home.model");
const About = require("../models/about.model");
const Skills = require("../models/skills.model");
const NewMessage = require("../models/newMessage.model");


const DB_URI = "mongodb://localhost:27017/andrea-campos";

const seedHome = [{
    name: "andrea",
    lastname: "campos.",
    description: "hi, I'm front end developer!",
}];

const seedAbout = [{
    "img": "https://drive.google.com/file/d/1QXSP0D-vqciRXGOEzAl3l21wPlkknUUo/view?usp=sharing",
    "title": "about andrea.",
    "description": "a year ago I changed the course of my life and decided to start studying web development. I'm currently training in the Upgrade-Hub bootcampt as Front End Developer, acquiring HTML, CSS, JavaScript, Angular and React skills, among others. Also I'm working as a Front End at Frenetic, a booming tech startup."
}];

const seedSkills = [{
        "img": "https://drive.google.com/file/d/19nwPhzSZYkEX1oxUEcnBGxu89nsXNpFM/view?usp=sharing",
        "title": "html",
        "description": "the structure of any website, handling Flexbox and Grid CSS Layout"
    },
    {
        "img": "https://drive.google.com/file/d/1akHKu3jcwA_pA9V5sA_zj61mXBtf9mlP/view?usp=sharing",
        "title": "css",
        "description": "to make cool your website with BEM, preprocessors like SASS and SCSS, bootstrap..."
    },
    {
        "img": "https://drive.google.com/file/d/1ec_u1LCO0iGWdCSjT3ywRy6uOjPIBdp9/view?usp=sharing",
        "title": "JavaScript",
        "description": "making your website dynamic since 1996 (ES6, OOP, FP...)"
    }, {
        "img": "https://drive.google.com/file/d/1du9B9Q4SJZqo0cdjjFaKxHmrZKGXzKVv/view?usp=sharing",
        "title": "Angular",
        "description": "your favorite framework (sorry React), using TypeScript life is better"
    }, {
        "img": "https://drive.google.com/file/d/1zPD_aFZfc5tcvWIuksE2n-eMwqrnHv6y/view?usp=sharing",
        "title": "React",
        "description": "to be continued..."
    }
];

const seedNewMessage = [{
    "name": "Andrea",
    "mail": "andrea.campos96@hotmail.com",
    "age": 24,
    "message": "Esto es una prueba para comprobar que el formulario post funciona :)",
}];

mongoose
    .connect(DB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async() => {
        console.log(`Connected to db ${DB_URI}`);

        console.log("Cleaning previous colletion...");
        await Home.deleteMany();
        await About.deleteMany();
        await Skills.deleteMany();
        await NewMessage.deleteMany();


        // Empiezo a trabajar a partir de este punto
        const homeInstances = seedHome.map((home) => new Home(home));
        await Home.insertMany(homeInstances);

        const aboutInstances = seedAbout.map((about) => new About(about));
        await About.insertMany(aboutInstances);

        const skillsInstances = seedSkills.map((skills) => new About(skills));
        await Skills.insertMany(skillsInstances);

        const newMessageInstances = seedNewMessage.map((newMessage) => new NewMessage(newMessage));
        await NewMessage.insertMany(newMessageInstances);


        console.log("Finish inserting Home");
        console.log("Finish inserting About");
        console.log("Finish inserting Skills");
        console.log("Finish posting newMessage");
        mongoose.connection.close();
    })
    .catch((err) => {
        console.log("There was an error connecting to the db");
        console.log(err);
    });