// Paro y me voy a este fichero -> model
const Skills = require("../models/skills.model");

const skillsController = {};

skillsController.getSkills = async(req, res, next) => {
    try {
        const allSkills = await Skills.find();
        res.status(200).json({
            data: allSkills,
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            data: "Could not find anything in Skills",
        });
    }
};

module.exports = skillsController;