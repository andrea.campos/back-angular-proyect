// Paro y me voy a este fichero -> model
const About = require("../models/about.model");

const aboutController = {};

aboutController.getAbout = async(req, res, next) => {
    try {
        const allAbout = await About.find();
        res.status(200).json({
            data: allAbout,
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            data: "Could not find anything in About",
        });
    }
};

module.exports = aboutController;