// Paro y me voy a este fichero -> model
const Home = require("../models/home.model");

const homeController = {};

homeController.getHome = async(req, res, next) => {
    try {
        const allHome = await Home.find();
        res.status(200).json({
            data: allHome,
        });
    } catch (err) {
        console.log(err);
        res.status(500).json({
            data: "Could not find anything in Home",
        });
    }
};

module.exports = homeController;