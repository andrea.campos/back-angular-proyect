const express = require("express");
const router = express.Router();

// para y se va a este fichero -> controller
const newMessageController = require("../controllers/newMessage.controller");

// router.get("/", newMessageController.postNewMessage);
// router.get("/:id", newMessageController.postNewMessageId);

router.post("/", newMessageController.postNewMessage);
router.post("/:id", newMessageController.postNewMessageId);

module.exports = router;