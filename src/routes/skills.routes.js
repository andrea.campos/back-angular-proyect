const express = require("express");
const router = express.Router();

// para y se va a este fichero -> controller
const skillsController = require("../controllers/skills.controller");

router.get("/", skillsController.getSkills);

module.exports = router;