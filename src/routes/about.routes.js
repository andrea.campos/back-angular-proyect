const express = require("express");
const router = express.Router();

// para y se va a este fichero -> controller
const aboutController = require("../controllers/about.controller");

router.get("/", aboutController.getAbout);

module.exports = router;