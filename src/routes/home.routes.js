const express = require("express");

const router = express.Router();

// para y se va a este fichero -> controller
const homeController = require("../controllers/home.controller");

router.get("/", homeController.getHome);

module.exports = router;